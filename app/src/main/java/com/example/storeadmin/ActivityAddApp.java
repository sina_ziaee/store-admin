package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class ActivityAddApp extends AppCompatActivity {

    EditText et_name;
    EditText et_rate;
    EditText et_size;
    EditText et_description;
    EditText et_category;
    ImageView img_image;

    ImageButton img_button;
    Button btn_add_item;

    FirebaseDatabase mFirebaseDatabase;
    FirebaseStorage mFirebaseStorage;
    StorageReference mItemPhotoReference;
    DatabaseReference mItemInformationReference;

    ProgressDialog dialog;

    private static final int RC_PHOTO_PICKER =  2;

    String photo_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_app);

        dialog = new ProgressDialog(this);
        dialog.setTitle("Uploading");
        dialog.setMessage("wait a sec please");
        dialog.setCancelable(false);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        setTitle("Add App");

        cast();
        setFirebaseInitializations();
        setListeners();
    }

    private void setFirebaseInitializations() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();
        mItemInformationReference = mFirebaseDatabase.getReference().child("items/apps");
        mItemPhotoReference = mFirebaseStorage.getReference().child("item_photos");
    }

    private void cast() {
        et_name = findViewById(R.id.et_name);
        et_name.setEnabled(true);
        et_category = findViewById(R.id.et_category);
        et_description = findViewById(R.id.et_description);
        et_rate = findViewById(R.id.et_rate);
        et_size = findViewById(R.id.et_size);
        img_button = findViewById(R.id.img_image_chooser);
        btn_add_item = findViewById(R.id.btn_add);
        img_image = findViewById(R.id.img_image_in_add_app);
    }

    private void setListeners() {
        img_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(intent, "Complete action using")
                ,RC_PHOTO_PICKER);
            }
        });

        btn_add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    private void register() {
        String name = et_name.getText().toString();
        String rate = et_rate.getText().toString();
        String size = et_size.getText().toString();
        String description = et_description.getText().toString();
        String category = et_category.getText().toString();
        String first = category.substring(0,1).toUpperCase();
        category = first + category.substring(1);


        Item_App item = new Item_App(name, rate, size,
                photo_url,category,description,0,1);

        mItemInformationReference.child(item.getName()).setValue(item);

        Toast.makeText(ActivityAddApp.this, "Uploaded", Toast.LENGTH_SHORT).show();

        et_name.setText("");
        et_rate.setText("");
        et_size.setText("");
        et_description.setText("");
        et_category.setText("");
        img_image.setImageURI(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        dialog.show();

        if (requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK){
            final Uri selectedImageUri = data.getData();
            final Uri[] downloadUrl = new Uri[1];
            final StorageReference photoRef =
                    mItemPhotoReference.child(selectedImageUri.getLastPathSegment());
            final UploadTask uploadTask = photoRef.putFile(selectedImageUri);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()){
                                throw task.getException();
                            }
                            return photoRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()){
                                downloadUrl[0] = task.getResult();

                                photo_url = downloadUrl[0].toString();
                                img_image.setImageURI(selectedImageUri);
                                Toast.makeText(ActivityAddApp.this, "Image uploaded", Toast.LENGTH_SHORT).show();
                                btn_add_item.setEnabled(true);

                                dialog.dismiss();

                            }
                        }
                    });
                }
            });
        }
    }
}
