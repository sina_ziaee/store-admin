package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ActivityAddMovie extends AppCompatActivity {

    String photo_url = "";
    String photo_url_in_firebase;

    ProgressBar dialog;
    RequestQueue mQueue;
    Context mContext;

    Button btn_add;
    Button btn_search;
    EditText et_search;
    EditText tv_name;
    EditText tv_director;
    EditText tv_rate;
    EditText tv_duration;
    EditText tv_plot;
    EditText tv_genre;
    EditText tv_year;
    EditText tv_actors;
    EditText tv_country;
    ImageView imageView;
    TextView tv_photo_url;
    RelativeLayout layout;

    Bitmap photo_bitmap;

    FirebaseDatabase mFirebaseDatabase;
    FirebaseStorage mFirebaseStorage;
    StorageReference mItemPhotoReference;
    DatabaseReference mItemInformationReference;

    ProgressDialog dialogProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);

        dialogProgress = new ProgressDialog(this);
        dialogProgress.setTitle("Uploading");
        dialogProgress.setMessage("wait a sec please");
        dialogProgress.setCancelable(false);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        setTitle("Add Movie");

        setFirebaseInitializations();
        cast();
        setListeners();
    }

    private void setFirebaseInitializations() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();
        mItemInformationReference = mFirebaseDatabase.getReference().child("items/movies");
        mItemPhotoReference = mFirebaseStorage.getReference().child("item_photos");
    }

    private void cast() {
        mContext = getApplicationContext();
        mQueue = Volley.newRequestQueue(mContext);

        tv_name = findViewById(R.id.tv_name_in_item_activity);
        tv_director = findViewById(R.id.tv_director_in_item_activity);
        tv_rate = findViewById(R.id.tv_rate_in_item_activity);
        tv_duration = findViewById(R.id.tv_size_in_item_activity);
        tv_genre = findViewById(R.id.tv_genre_in_item_activity);
        tv_plot = findViewById(R.id.tv_plot_in_item_activity);
        tv_actors = findViewById(R.id.tv_actors_in_item_activity);
        tv_country = findViewById(R.id.tv_country_in_item_activity);
        tv_year = findViewById(R.id.tv_year_in_item_activity);
        dialog = findViewById(R.id.progressBar_in_logon);
        imageView = findViewById(R.id.img_in_item_activity);
        et_search = findViewById(R.id.et_search_in_omdb);
        btn_search = findViewById(R.id.btn_search);
        btn_add = findViewById(R.id.btn_add_in_movie);
        tv_photo_url = findViewById(R.id.tv_photo_url);
        layout = findViewById(R.id.relative_in_movie);
    }

    private void setListeners() {
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendParamsGet(et_search.getText().toString());
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "clicked", Toast.LENGTH_SHORT).show();
                uploadPhoto();
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 3) {
                    btn_search.setClickable(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void saveDataInFirebaseDatabase() {
        String name = tv_name.getText().toString().toLowerCase();
        String rate = tv_rate.getText().toString();
        String genres = tv_genre.getText().toString();
        String director = tv_director.getText().toString();
        String duration = tv_duration.getText().toString();
        String plot = tv_plot.getText().toString();
        String year = tv_year.getText().toString();
        String actors = tv_actors.getText().toString();
        String countries = tv_country.getText().toString();
        String[] category = tv_genre.getText().toString().split(", ");

        Item_Movie item = new Item_Movie(name, genres, rate,
                duration, year,director,countries,
                actors,plot,photo_url_in_firebase, category[0],0,2);
        mItemInformationReference.child(item.getName()).setValue(item);
//        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("views/movies");
//        dbRef.push().setValue(item);

        tv_name.setText("");
        tv_rate.setText("");
        tv_genre.setText("");
        tv_director.setText("");
        tv_duration.setText("");
        tv_plot.setText("");
        tv_year.setText("");
        tv_actors.setText("");
        tv_country.setText("");
        et_search.setText("");

        Toast.makeText(mContext, "information uploaded on database", Toast.LENGTH_SHORT).show();

    }

    private void uploadPhoto() {

        dialogProgress.show();
        Uri selectedImageUri = loadImageFromStorage(photo_url);
        final Uri[] downloadUrl = new Uri[1];
        final StorageReference photoRef =
                mItemPhotoReference.child(selectedImageUri.getLastPathSegment());
        final UploadTask uploadTask = photoRef.putFile(selectedImageUri);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return photoRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            dialogProgress.dismiss();

                            downloadUrl[0] = task.getResult();

                            photo_url_in_firebase = downloadUrl[0].toString();

                            Toast.makeText(ActivityAddMovie.this, "Image uploaded", Toast.LENGTH_SHORT).show();
                            btn_add.setClickable(true);

                            saveDataInFirebaseDatabase();

                        }
                    }
                });
            }
        });

    }

    public void sendParamsGet(String name) {
        String url = "https://www.omdbapi.com/?apikey=ba3185c&t=" + name;

        dialog.setVisibility(View.VISIBLE);
        dialogProgress.show();
        layout.setVisibility(View.INVISIBLE);
        JsonObjectRequest request = new JsonObjectRequest(JsonObjectRequest.Method.GET, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    final String name = response.getString("Title");
                    String year = response.getString("Year");
                    String rate = response.getString("imdbRating");
                    String plot = response.getString("Plot");
                    String director = response.getString("Director");
                    String genre = response.getString("Genre");
                    String img_resource_url = response.getString("Poster");
                    String size = response.getString("Runtime");
                    String country = response.getString("Country");
                    String actors = response.getString("Actors");
                    String img_url = response.getString("Poster");

                    Log.v("check", name + " " + director);
                    tv_name.setText(name);
                    tv_director.setText(director);
                    tv_rate.setText(rate);
                    tv_plot.setText(plot);
                    tv_genre.setText(genre);
                    tv_duration.setText(size);
                    tv_year.setText(year);
                    tv_country.setText(country);
                    tv_actors.setText(actors);
                    tv_photo_url.setText(img_url);

                    ImageRequest request_img = new ImageRequest(img_resource_url, new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            photo_bitmap = response;
                            imageView.setImageBitmap(response);
                            dialog.setVisibility(View.INVISIBLE);
                            dialogProgress.dismiss();
                            layout.setVisibility(View.VISIBLE);
                            btn_add.setClickable(true);

                            saveImage(response, name);

                        }
                    }, 120, 120, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.ARGB_8888,
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    dialogProgress.dismiss();
                                    dialog.setVisibility(View.INVISIBLE);
                                    Toast.makeText(ActivityAddMovie.this, "Error : " + error.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                }
                            });

                    mQueue.add(request_img);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    Toast.makeText(mContext, "Bad Name", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);

    }

    private void saveImage(Bitmap bitmapImage, String name) {

        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, name + ".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            Toast.makeText(cw, "saved", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        photo_url = mypath.getAbsolutePath();

    }


    private Uri loadImageFromStorage(String path) {

        File f = new File(path);
        return Uri.fromFile(f);

    }

}
