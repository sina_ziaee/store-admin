package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ActivityAddNews extends AppCompatActivity {

    EditText et_title;
    EditText et_content;
    Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);

        setTitle("Add News");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        et_title = findViewById(R.id.et_title);
        et_content = findViewById(R.id.et_content);
        btn_submit = findViewById(R.id.btn_add_news);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = et_title.getText().toString();
                String content = et_content.getText().toString();

                if (title.length() < 5){
                    Toast.makeText(ActivityAddNews.this, "Please set a title", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (content.length() < 10){
                    Toast.makeText(ActivityAddNews.this, "Please set a content", Toast.LENGTH_SHORT).show();
                    return;
                }

                sendNews(title, content);

            }
        });

    }

    private void sendNews(String title, String content) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("news");

        Item_message message = new Item_message(title, content);

        ref.push().setValue(message).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(ActivityAddNews.this, "News added", Toast.LENGTH_SHORT).show();
                    et_content.setText("");
                    et_title.setText("");
                }
                else{
                    Toast.makeText(ActivityAddNews.this, "Failed upload news", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
