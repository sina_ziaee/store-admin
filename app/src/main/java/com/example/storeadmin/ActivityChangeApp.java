package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class ActivityChangeApp extends AppCompatActivity {

    EditText et_name;
    EditText et_rate;
    EditText et_size;
    EditText et_description;
    EditText et_category;
    ImageView img_image;

    ImageButton img_button;
    Button btn_update_item;
    LinearLayout container;
    RecyclerView recyclerView;
    TextView tv_views;

    AdapterComment adapter;
    String key;

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("items/apps");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_app);

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        Toast.makeText(this, key, Toast.LENGTH_SHORT).show();
        ref = ref.child(key);

        cast();

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        setTitle("Update App");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Item_App item =  dataSnapshot.getValue(Item_App.class);
                et_name.setText(item.getName());
                et_category.setText(item.getCategory());
                et_size.setText(String.valueOf(item.getSize()));
                et_description.setText(item.getDescription());
                et_rate.setText(String.valueOf(item.getRate()));
                tv_views.setText(item.getViews()+"");
                Glide.with(getApplicationContext()).load(item.getPhoto_url()).into(img_image);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        setUpRecyclerView();

    }

    private void setUpRecyclerView() {
        Query query = FirebaseDatabase.getInstance().getReference().child("comments/" + key).limitToFirst(5);

        FirebaseRecyclerOptions<Item_comment> options = new FirebaseRecyclerOptions.Builder<Item_comment>()
                .setQuery(query, Item_comment.class)
                .build();

        adapter = new AdapterComment(options);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper((new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                adapter.deleteItem(viewHolder.getAdapterPosition());
                Toast.makeText(ActivityChangeApp.this, "Comment Removed", Toast.LENGTH_SHORT).show();
            }
        })).attachToRecyclerView(recyclerView);

    }

    private void cast() {
        tv_views = findViewById(R.id.views_app);
        recyclerView = findViewById(R.id.rv_comments);
        container = findViewById(R.id.linearLayout_container);
        container.setVisibility(View.VISIBLE);
        et_name = findViewById(R.id.et_name);
        et_category = findViewById(R.id.et_category);
        et_description = findViewById(R.id.et_description);
        et_rate = findViewById(R.id.et_rate);
        et_size = findViewById(R.id.et_size);
        img_button = findViewById(R.id.img_image_chooser);
        btn_update_item = findViewById(R.id.btn_add);
        img_image = findViewById(R.id.img_image_in_add_app);
        img_button.setVisibility(View.GONE);
        btn_update_item.setText("Update");
        btn_update_item.setEnabled(true);
        btn_update_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> itemUpdates = new HashMap<>();

                itemUpdates.put("category", et_category.getText().toString());
                itemUpdates.put("description", et_description.getText().toString());
                itemUpdates.put("name", et_name.getText().toString());
                itemUpdates.put("rate", et_rate.getText().toString());
                itemUpdates.put("size", et_size.getText().toString());

                ref.updateChildren(itemUpdates);

                Toast.makeText(ActivityChangeApp.this, "updated", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
