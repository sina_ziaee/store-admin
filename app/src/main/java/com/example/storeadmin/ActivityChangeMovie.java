package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class ActivityChangeMovie extends AppCompatActivity {

    Button btn_update;
    Button btn_search;
    EditText et_search;
    EditText tv_name;
    EditText tv_director;
    EditText tv_rate;
    EditText tv_duration;
    EditText tv_plot;
    EditText tv_genre;
    EditText tv_year;
    EditText tv_actors;
    EditText tv_country;
    ImageView imageView;
    TextView tv_photo_url;
    RelativeLayout layout;
    LinearLayout layout2;
    LinearLayout comments_container;
    RecyclerView recyclerView;
    TextView tv_views;

    AdapterComment adapter;
    String key;

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("items/movies");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);

        cast();

        setTitle("Update Movie");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        Toast.makeText(this, key, Toast.LENGTH_SHORT).show();

        ref = ref.child(key);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Item_Movie item =  dataSnapshot.getValue(Item_Movie.class);
                tv_name.setText(item.getName());
                tv_genre.setText(item.getGenres());
                tv_duration.setText(String.valueOf(item.getDuration()));
                tv_director.setText(item.getDirector());
                tv_rate.setText(String.valueOf(item.getRate()));
                tv_actors.setText(item.getActors());
                tv_plot.setText(item.getPlot());
                tv_country.setText(item.getCountries());
                tv_year.setText(String.valueOf(item.getYear()));
                tv_views.setText(item.getViews() + "");
                Glide.with(getApplicationContext()).load(item.getPhoto_url()).into(imageView);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        setUpRecyclerView();

    }

    private void setUpRecyclerView() {
        Query query = FirebaseDatabase.getInstance().getReference().child("comments/" + key).limitToFirst(5);

        FirebaseRecyclerOptions<Item_comment> options = new FirebaseRecyclerOptions.Builder<Item_comment>()
                .setQuery(query, Item_comment.class)
                .build();

        adapter = new AdapterComment(options);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper((new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                adapter.deleteItem(viewHolder.getAdapterPosition());
                Toast.makeText(ActivityChangeMovie.this, "Comment Removed", Toast.LENGTH_SHORT).show();
            }
        })).attachToRecyclerView(recyclerView);
    }

    private void cast() {
        tv_views = findViewById(R.id.views_app);
        recyclerView = findViewById(R.id.rv_comments_2);
        comments_container = findViewById(R.id.linearLayout_container2);
        comments_container.setVisibility(View.VISIBLE);
        tv_name = findViewById(R.id.tv_name_in_item_activity);
        tv_views.setEnabled(false);
        tv_director = findViewById(R.id.tv_director_in_item_activity);
        tv_rate = findViewById(R.id.tv_rate_in_item_activity);
        tv_duration = findViewById(R.id.tv_size_in_item_activity);
        tv_genre = findViewById(R.id.tv_genre_in_item_activity);
        tv_plot = findViewById(R.id.tv_plot_in_item_activity);
        tv_actors = findViewById(R.id.tv_actors_in_item_activity);
        tv_country = findViewById(R.id.tv_country_in_item_activity);
        tv_year = findViewById(R.id.tv_year_in_item_activity);
        imageView = findViewById(R.id.img_in_item_activity);
        et_search = findViewById(R.id.et_search_in_omdb);
        btn_search = findViewById(R.id.btn_search);
        btn_update = findViewById(R.id.btn_add_in_movie);
        tv_photo_url = findViewById(R.id.tv_photo_url);
        layout = findViewById(R.id.relative_in_movie);
        layout.setVisibility(View.VISIBLE);
        layout2 = findViewById(R.id.LinearLayout_search_container);
        layout2.setVisibility(View.GONE);
        btn_update.setText("Update");
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> itemUpdates = new HashMap<>();

                itemUpdates.put("actors",tv_actors.getText().toString());
                itemUpdates.put("countries",tv_country.getText().toString());
                itemUpdates.put("director",tv_director.getText().toString());
                itemUpdates.put("duration",tv_duration.getText().toString());
                itemUpdates.put("genres",tv_genre.getText().toString());
                itemUpdates.put("name",tv_name.getText().toString());
                itemUpdates.put("plot",tv_plot.getText().toString());
                itemUpdates.put("rate",tv_rate.getText().toString());
                itemUpdates.put("year",tv_year.getText().toString());

                ref.updateChildren(itemUpdates);

                Toast.makeText(ActivityChangeMovie.this, "updated", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
