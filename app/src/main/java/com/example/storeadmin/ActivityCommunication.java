package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ActivityCommunication extends AppCompatActivity {


    AdapterMessage adapter;

    ImageView img_send;
    ImageView img_image_chooser;
    EditText et_content;
    RecyclerView recyclerView;
    static ProgressBar progressBar;

    String username = "Admin";
    String karbar;

    DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference("messages");

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comunication);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        Intent intent = getIntent();
        karbar = intent.getStringExtra("username");

        setTitle(karbar);

        messageRef = messageRef.child(karbar);
        cast();
        setListeners();
        setMessageRecyclerView();
    }

    private void setMessageRecyclerView() {
        Query query= messageRef.orderByKey();
        FirebaseRecyclerOptions<Item_message> options = new FirebaseRecyclerOptions.Builder<Item_message>()
                .setQuery(query,Item_message.class)
                .build();

        int bgColor = ContextCompat.getColor(this, R.color.light_green);
        adapter = new AdapterMessage(options, bgColor);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }

    private void cast() {
        img_send = findViewById(R.id.img_send_in_comu);
        img_image_chooser = findViewById(R.id.img_image_picker_in_comu);
        et_content = findViewById(R.id.et_content_in_comu);
        recyclerView = findViewById(R.id.recycler_view_in_communication_activity);
        progressBar = findViewById(R.id.progress_communication);

    }

    private void setListeners() {

        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String content = et_content.getText().toString();
                Item_message item = new Item_message(username, content);
                messageRef.push().setValue(item).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ActivityCommunication.this, "message sent", Toast.LENGTH_SHORT).show();
                    }
                });

                et_content.setText("");
            }
        });

        et_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence.toString().trim().length() > 0) {
                    img_send.setEnabled(true);
                } else {
                    img_send.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        if (et_content.isFocused()){
            super.onBackPressed();
        }
        else{
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
