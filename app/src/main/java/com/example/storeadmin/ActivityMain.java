package com.example.storeadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

public class ActivityMain extends AppCompatActivity {

    SharedPreferences preferences;

    Button btn_chat;
//    Button btn_send_note;
    Button btn_update_item;
    Button btn_add_app;
    Button btn_add_movie;
    Button btn_add_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cast();
        setListeners();
    }

    private void cast() {
        preferences = getSharedPreferences("is_signed_in" ,MODE_PRIVATE);
        boolean is_admin_signed_in = preferences.getString("admin_username", "-1").equals("-1");
        if (!is_admin_signed_in){
            // ok
        }
        else{
            startActivity(new Intent(ActivityMain.this, AdminLogin.class));
        }

        btn_chat = findViewById(R.id.btn_chat);
        btn_add_app = findViewById(R.id.btn_add_app);
//        btn_send_note = findViewById(R.id.btn_send_notification);
        btn_update_item = findViewById(R.id.btn_update_item);
        btn_add_movie = findViewById(R.id.btn_add_movie);
        btn_add_news = findViewById(R.id.btn_news_item);
    }

    private void setListeners() {
        btn_add_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityApp.class));
            }
        });

        btn_add_movie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityMovie.class));
            }
        });

        btn_update_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityUsersList.class));
            }
        });

        btn_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityUsersCommunication.class));
            }
        });

//        btn_send_note.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        btn_add_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityNews.class));
            }
        });
    }

}
