package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ActivityNews extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerView;
    AdapterMessage adapter;
    FloatingActionButton button;
    static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        setTitle("News");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        recyclerView = findViewById(R.id.recycler_view_news);
        button = findViewById(R.id.btn_add_news);

        progressBar = findViewById(R.id.progressbar_in_news);

        button.setOnClickListener(this);

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("news");

        Query query = dbRef;

        FirebaseRecyclerOptions<Item_message> options = new FirebaseRecyclerOptions.Builder<Item_message>()
                .setQuery(query, Item_message.class)
                .build();

        int bgColor = ContextCompat.getColor(this, R.color.newsColor);

        adapter = new AdapterMessage(options, bgColor);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                adapter.deleteItem(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(recyclerView);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(ActivityNews.this, ActivityAddNews.class));
    }
}
