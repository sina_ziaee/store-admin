package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ActivityUser extends AppCompatActivity {

    TextView tv_username, tv_phone, tv_email, tv_name, tv_password;
    ImageView image;
    String key;

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        ref = ref.child(key);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        setTitle(key);

        cast();

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user.getName() != null){
                    tv_name.setText(user.getName());
                }
                tv_phone.setText(user.getPhoneNumber());
                tv_username.setText(user.username);
                tv_email.setText(user.getEmail());
                tv_password.setText(user.getPassword());
                if (user.getPhotoURL() != null){
                    Glide.with(getApplicationContext()).load(user.getPhotoURL()).into(image);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void cast() {
        image = findViewById(R.id.img_profile);
        tv_name = findViewById(R.id.tv_fullname);
        tv_email = findViewById(R.id.tv_email);
        tv_password = findViewById(R.id.tv_password);
        tv_phone = findViewById(R.id.tv_phone);
        tv_username = findViewById(R.id.tv_username_in_user);
    }
}
