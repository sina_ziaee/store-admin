package com.example.storeadmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ProgressBar;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ActivityUsersList extends AppCompatActivity {

    RecyclerView recyclerView;
    AdapterUserList adapter;
    static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        setTitle("Users");

        cast();
        setUpRecyclerView();

    }

    private void cast() {
        recyclerView = findViewById(R.id.users_list);
        progressBar = findViewById(R.id.progress_users);
    }

    private void setUpRecyclerView() {

        Query query = FirebaseDatabase.getInstance().getReference("users");

        FirebaseRecyclerOptions<User> options = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();

        adapter = new AdapterUserList(options);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }
}
