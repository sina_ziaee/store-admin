package com.example.storeadmin;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.Holder>{

    Context mContext;
    ArrayList<String> users_list;

    public Adapter(ArrayList<String> users_list) {
        this.users_list = users_list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent,
                false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        String username = users_list.get(position);
        holder.tv_username.setText(username);
    }

    @Override
    public int getItemCount() {
        return users_list.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_username;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_username = itemView.findViewById(R.id.tv_username_user);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String username = tv_username.getText().toString();

            Intent intent = new Intent(mContext, ActivityCommunication.class);
            intent.putExtra("username", username);
            mContext.startActivity(intent);
        }
    }

}
