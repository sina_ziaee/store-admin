package com.example.storeadmin;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.core.Path;

public class AdapterApp extends FirebaseRecyclerAdapter<Item_App, AdapterApp.Holder> {

    Context mContext;

    public AdapterApp(@NonNull FirebaseRecyclerOptions<Item_App> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull Holder holder, int position, @NonNull Item_App model) {
        holder.tv_title.setText(model.getName());
        holder.tv_rate.setText(String.valueOf(model.getRate()));
        Glide.with(mContext).load(model.getPhoto_url()).into(holder.img_image);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,
                parent, false);
        return new Holder(view);
    }

    public void deleteItem(int position){
        getSnapshots().getSnapshot(position).getRef().removeValue();
        // you need to delete from storage too
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_title;
        TextView tv_rate;
        ImageView img_image;
        ImageView img_delete;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_name_item);
            tv_rate = itemView.findViewById(R.id.tv_rate_item);
            img_image = itemView.findViewById(R.id.img_image_item);
            img_delete = itemView.findViewById(R.id.img_delete_item);
            mContext = itemView.getContext();
            img_delete.setOnClickListener(this);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();

            if (v.getId() == img_delete.getId()){
                deleteItem(position);
            }
            else if (v.getId() == itemView.getId()){
                if (position != RecyclerView.NO_POSITION){
                    DatabaseReference dbRef = getSnapshots().getSnapshot(position).getRef();
                    String key = dbRef.getKey();
                    
                    Intent intent = new Intent(mContext, ActivityChangeApp.class);
                    intent.putExtra("key", key);
                    mContext.startActivity(intent);
                }
            }
        }
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        ActivityApp.progressBar.setVisibility(View.GONE);
    }
}
