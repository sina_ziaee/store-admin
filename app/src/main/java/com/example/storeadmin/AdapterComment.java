package com.example.storeadmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class AdapterComment extends FirebaseRecyclerAdapter<Item_comment, AdapterComment.Holder> {

    Context mContext;

    public AdapterComment(@NonNull FirebaseRecyclerOptions<Item_comment> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull Holder holder, int position, @NonNull Item_comment model) {
        holder.tv_username.setText(model.getUsername());
        holder.tv_comment.setText(model.getComment());
        holder.ratingBar.setRating(model.getRating());
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new Holder(view);
    }

    public void deleteItem(int position){
        getSnapshots().getSnapshot(position).getRef().removeValue();
        // you need to delete from storage too
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView tv_username;
        TextView tv_comment;
        RatingBar ratingBar;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_comment = itemView.findViewById(R.id.tv_comment);
            tv_username = itemView.findViewById(R.id.tv_username);
            ratingBar = itemView.findViewById(R.id.rating_bar_app);
            mContext = itemView.getContext();
        }
    }

}
