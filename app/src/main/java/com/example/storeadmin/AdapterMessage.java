package com.example.storeadmin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class AdapterMessage extends FirebaseRecyclerAdapter<Item_message, AdapterMessage.Holder> {

    int bgColor;

    public AdapterMessage(@NonNull FirebaseRecyclerOptions<Item_message> options, int bgColor) {
        super(options);
        this.bgColor = bgColor;
    }

    public void deleteItem(int position){
        getSnapshots().getSnapshot(position).getRef().removeValue();
        // you need to delete from storage too
    }

    @Override
    protected void onBindViewHolder(@NonNull Holder holder, int position, @NonNull Item_message model) {
        String username = model.getUsername();
        if (!username.equals("Admin")){
            holder.container.setBackgroundColor(bgColor);
            holder.container.setRadius(10);
            holder.container.setElevation(3);
        }
        holder.tv_content.setText(model.getContent());
        holder.tv_username.setText(username);

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message,
                parent, false);
        return new Holder(view);
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tv_username;
        TextView tv_content;
        CardView container;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_username = itemView.findViewById(R.id.tv_username_message);
            tv_content = itemView.findViewById(R.id.tv_content_message);
            container = itemView.findViewById(R.id.message_container);
        }
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        if (ActivityCommunication.progressBar == null && ActivityNews.progressBar != null){
            ActivityNews.progressBar.setVisibility(View.GONE);
        }
        else{
            ActivityCommunication.progressBar.setVisibility(View.GONE);
        }
    }
}
