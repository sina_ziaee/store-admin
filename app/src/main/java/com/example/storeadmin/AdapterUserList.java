package com.example.storeadmin;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterUserList extends FirebaseRecyclerAdapter<User, AdapterUserList.Holder> {

    Context mContext;

    public AdapterUserList(@NonNull FirebaseRecyclerOptions<User> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull Holder holder, int position, @NonNull User model) {
        if (model.getPhotoURL() != null){
            Glide.with(mContext).load(model.getPhotoURL()).into(holder.image);
        }
        holder.tv_username.setText(model.getUsername());
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user, parent, false);
        return new Holder(view);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_username;
        CircleImageView image;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_username = itemView.findViewById(R.id.tv_user_username);
            image = itemView.findViewById(R.id.img_user_image);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ActivityUser.class);
            int position = getAdapterPosition();
            DatabaseReference dbRef = getSnapshots().getSnapshot(position).getRef();
            String key = dbRef.getKey();
            intent.putExtra("key", key);
            mContext.startActivity(intent);
        }
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        ActivityUsersList.progressBar.setVisibility(View.GONE);
    }
}
