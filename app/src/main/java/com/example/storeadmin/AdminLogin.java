package com.example.storeadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AdminLogin extends AppCompatActivity {


    EditText et_email;
    EditText et_password;
    EditText et_username;
    CheckBox checkBox;
    Button btn_sign_in;
    TextView tv_go_to_sign_up;
    ImageView img_google;
    ImageView img_facebook;
    ImageView img_twitter;


    SharedPreferences appPreferences;
    SharedPreferences.Editor editor;

    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        mFirebaseAuth = FirebaseAuth.getInstance();
        cast();
        setListeners();
        appPreferences = getSharedPreferences("is_signed_in" ,MODE_PRIVATE);
        Toast.makeText(this, "username : " + appPreferences.getString("admin_username",
                "not set"), Toast.LENGTH_LONG).show();
    }

    private void cast() {
        et_password = findViewById(R.id.et_password_in_login);
        et_email = findViewById(R.id.et_email_in_login);
        checkBox = findViewById(R.id.checkbox_in_login);
        btn_sign_in = findViewById(R.id.btn_signin);
        tv_go_to_sign_up = findViewById(R.id.tv_goto_sign_up);
        img_facebook = findViewById(R.id.img_facebook_in_sign_in);
        img_google = findViewById(R.id.img_gogoleplus_in_singin);
        img_twitter = findViewById(R.id.img_twitter_in_signin);
        et_username = findViewById(R.id.et_username_in_login);
    }

    private void setListeners() {

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
        img_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        img_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        img_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void register() {
        final String email = et_email.getText().toString();
        String password = et_password.getText().toString();
        final String username = et_username.getText().toString();

        if (email.isEmpty()){
            et_email.setError("Email required");
            et_password.requestFocus();
            return;
        }

        if (password.isEmpty()){
            et_password.setError("Password required");
            et_password.requestFocus();
            return;
        }

        if (username.isEmpty()){
            et_username.setError("Username required");
            et_username.requestFocus();
            return;
        }

        mFirebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(AdminLogin.this, "Signed in", Toast.LENGTH_SHORT).show();
                            editor = appPreferences.edit();
                            editor.putString("admin_email", email);
                            editor.putString("admin_username", username);
                            editor.apply();
                            startActivity(new Intent(AdminLogin.this, ActivityMain.class));
                            finish();
                        }
                        else{
                            Toast.makeText(AdminLogin.this, "Failed " + task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    @Override
    public void onBackPressed() {
        if (!appPreferences.getString("admin_username", "-1").equals("-1")){
            super.onBackPressed();
        }
        else{
            Toast.makeText(this, "Please log in first", Toast.LENGTH_SHORT).show();
        }
    }
}
