package com.example.storeadmin;

import java.util.ArrayList;

public class Item_App {
    public String name;
    public String rate;
    public String size;
    public String photo_url;
    public String category;
    public String description;
    int views;
    int type;
    public Item_App(){

    }

    public Item_App(String name, String rate, String size,
                    String photo_url, String category,
                    String description, int views, int type) {
        this.name = name;
        this.rate = rate;
        this.size = size;
        this.photo_url = photo_url;
        this.category = category;
        this.description = description;
        this.views = views;
        this.type = type;
    }

    public int getViews() {
        return views;
    }

    public String getName() {
        return name;
    }

    public String getRate() {
        return rate;
    }

    public String getSize() {
        return size;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public int getType() {
        return type;
    }
}
