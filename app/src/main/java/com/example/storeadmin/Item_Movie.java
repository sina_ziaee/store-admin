package com.example.storeadmin;

import java.util.ArrayList;

public class Item_Movie {
    public String name;
    public String genres;
    public String rate;
    public String duration;
    public String year;
    public String director;
    public String countries;
    public String actors;
    public String plot;
    public String photo_url;
    public String category;
    public int views;
    public int type;

    public Item_Movie(){

    }

    public String getName() {
        return name;
    }

    public String getGenres() {
        return genres;
    }

    public String getRate() {
        return rate;
    }

    public String getDuration() {
        return duration;
    }

    public String getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

    public String getCountries() {
        return countries;
    }

    public String getActors() {
        return actors;
    }

    public String getPlot() {
        return plot;
    }

    public String getCategory() {
        return category;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public int getViews() {
        return views;
    }

    public int getType() {
        return type;
    }

    public Item_Movie(String name, String genres, String rate, String duration,
                      String year, String director, String countries, String actors,
                      String plot, String photo_url, String category, int views, int type) {
        this.name = name;
        this.genres = genres;
        this.rate = rate;
        this.duration = duration;
        this.year = year;
        this.director = director;
        this.countries = countries;
        this.actors = actors;
        this.plot = plot;
        this.photo_url = photo_url;
        this.category = category;
        this.views = views;
        this.type = type;
    }
}

