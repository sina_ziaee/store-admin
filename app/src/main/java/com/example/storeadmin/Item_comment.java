package com.example.storeadmin;

public class Item_comment {

    String username;
    String comment;
    float rating;

    public Item_comment(String username, String comment, float rating) {
        this.username = username;
        this.comment = comment;
        this.rating = rating;
    }

    public float getRating() {
        return rating;
    }

    public Item_comment() {

    }

    public String getUsername() {
        return username;
    }

    public String getComment() {
        return comment;
    }
}
