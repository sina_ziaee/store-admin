package com.example.storeadmin;

public class Item_message {

    public String username;
    public String content;

    public Item_message(String username, String content) {
        this.username = username;
        this.content = content;
    }

    public Item_message() {

    }

    public String getUsername() {
        return username;
    }

    public String getContent() {
        return content;
    }
}
