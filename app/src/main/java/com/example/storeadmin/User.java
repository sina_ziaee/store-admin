package com.example.storeadmin;

import java.util.ArrayList;

public class User {
    public String name, email, password, username;
    public ArrayList<String> downloads;
    public String photoURL, phoneNumber;

    public User() {
    }

    public User(String name, String email, String password,
                String username, ArrayList<String> downloads,
                String photoURL, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.username = username;
        this.downloads = downloads;
        this.photoURL = photoURL;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ArrayList<String> getDownloads() {
        return downloads;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }
}
